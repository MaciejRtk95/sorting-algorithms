void bubbleSort(int array[], int length) {
    int swapBuffer;
    bool swappedAnything;

    do{
        swappedAnything = false;
        for (int i = 0; i < length - 1; i++) {
            if (array[i] > array[i + 1]) {
                swapBuffer = array[i + 1];
                array[i + 1] = array[i];
                array[i] = swapBuffer;
                swappedAnything = true;
            }
        }
        length--;
    } while (swappedAnything);
}