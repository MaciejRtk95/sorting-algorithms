void quicksort(int array[], int left, int right) {
    int low = left;
    int high = right;
    int pivot = array[(left + right) / 2];
    int swapBuffer;

    while (low <= high) {
        while (array[low] < pivot)
            low++;
        while (array[high] > pivot)
            high--;

        if (low <= high) {
            if (low != high) {
                swapBuffer = array[low];
                array[low] = array[high];
                array[high] = swapBuffer;
            }
            //Here we avoid infinite loops caused by equal values (of both the markers and what they're marking)
            low++;
            high--;
        }
    }

    if (left < high)
        quicksort(array, left, high);
    if (low < right)
        quicksort(array, low, right);
}
