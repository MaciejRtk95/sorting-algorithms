# Sorting algorithms

As the very original name implies, this repository consists of sorting algorithms. Specifically of sorting algorithms written in C++.

Current algorithms on board are:
- Bubble sort
- Insertion sort
- Quicksort
- Selection sort

Algorithms that are planned to be added are:
- Merge sort
- Heapsort
- Introsort
- Timsort
- Cubesort
- Shell sort
- Binary tree sort
- Cycle sort
- Library sort
- Patience sorting
- Smoothsort
- Tournament sort
- Coctail sort
- Comb sort
- Gnome sort
- Block sort
- Odd-even sort
- Pigeonhole sort
- Bucket sort
- Counting sort
- Radix sort
- Spreadsort
- Burstsort
- Flashsort
- Postman sort
- Bead sort
- Simple pancake sort
- Spaghetti sort
- Sorting network
- Bitonic sorter
- Bogosort
- Stooge sort
