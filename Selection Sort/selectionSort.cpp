void selectionSort(int array[], int length) {
    for (int i = 0; i < length - 1; i++) {
        int lowest = i;
        for (int j = i + 1; j < length; j++)
            if (array[j] < array[lowest])
                lowest = j;

        if (lowest != i) {
            int swapBuffer = array[lowest];
            array[lowest] = array[i];
            array[i] = swapBuffer;
        }
    }
}